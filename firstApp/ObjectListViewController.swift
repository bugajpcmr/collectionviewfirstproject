//
//  ObjectListViewController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 08/08/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit
import SDWebImage

class ObjectListViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let advancedGaleryCellId = "advancedgalerycell"
    
    let photos = ["room_1", "room_2", "object_2", "object_1", "room_1"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWithNewData), name: NSNotification.Name(rawValue: "load"), object: nil)

        ObjectManager.getObjects()
        setupCollectionView()
        registerCells()
    }
    @objc func reloadWithNewData(){
        collectionView.reloadData()
    }
    
    private func setupCollectionView(){
        navigationItem.title = "Obiekty"
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Filter", style: .done,
            target: self,
            action: #selector(filterTapped)
        )
        navigationItem.rightBarButtonItem?.tintColor = .customOrange
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 16, right: 0)
        //navigationItem.backBarButtonItem = .none
    }
    
    @objc func filterTapped(){
        let desVC = FilterViewController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(desVC, animated: true)
    }
    
    //View change
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        let objectTitle = ObjectsArray[indexPath.row].title
        let alert = UIAlertController(title: "Usuń obiekt", message: "Czy na pewno usunąć obiekt \(objectTitle)?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Usuń", style: UIAlertAction.Style.destructive, handler: { action in
            ObjectManager.deleteItem(id: ObjectsArray[indexPath.row].id, title: objectTitle)
            self.showToast(message: "Usunięto obiekt: \(objectTitle)", font: .boldSystemFont(ofSize: 14))
        }))
        alert.addAction(UIAlertAction(title: "Anuluj", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(AdvancedGaleryCell.self, forCellWithReuseIdentifier: advancedGaleryCellId)
    }
    
    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: advancedGaleryCellId, for: indexPath) as! AdvancedGaleryCell
//            cell.imageView.image = UIImage(named: ObjectsArray[indexPath.row].url)
            cell.myCollectionViewController = self
            cell.imageView.sd_setImage(with: URL(string: ObjectsArray[indexPath.row].url), placeholderImage: UIImage(named: "room_1"))
            cell.imageTitleLabel.text = "\(ObjectsArray[indexPath.row].driveTime) minut drogi"
            if ObjectsArray[indexPath.row].isFeatured == 1 {
                cell.iconHeartImage.setImage(UIImage(named: "heart_solid"), for: .normal)
            } 
            cell.titleLabel.text = ObjectsArray[indexPath.row].title
            cell.priceLabel.text = "\(ObjectsArray[indexPath.row].price) zł/os"
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func tapFavouriteIcon(cell: UICollectionViewCell){
        
        print("click")
        
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        ObjectManager.getObjects()
        if ObjectsArray[indexPath.row].isFeatured == 1{
            ObjectsArray[indexPath.row].isFeatured = 0
        } else {
            ObjectsArray[indexPath.row].isFeatured = 1
        }
        
        //jak zaktualizować coredata
        //collectionView.reloadItems(at: [indexPath])
        collectionView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadArray"), object: nil)
    }
    
    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return ObjectsArray.count
        default:
            return 0
        }
    }
    
    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width, height: 245)
        default:
            return CGSize(width: view.frame.width, height: 245)
        }
    }
    
    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}



