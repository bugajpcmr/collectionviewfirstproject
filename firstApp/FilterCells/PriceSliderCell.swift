//
//  PriceSliderCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 01/08/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit
import RangeSeekSlider
import Foundation

class PriceSliderCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
//    let rangeSlider: RangeSeekSlider = {
//        let slider = RangeSeekSlider()
//        //slider.disableRange = true
//        slider.maxValue = 100
//        slider.selectedMaxValue = 100
//        slider.handleDiameter = 28
//        slider.selectedHandleDiameterMultiplier = 1.2
//        slider.tintColor = UIColor(r: 199, g: 199, b: 204, a: 1)
//        slider.handleColor = .white
//        slider.colorBetweenHandles = UIColor(r: 56, g: 204, b: 102, a: 1) //green
//        slider.minLabelFont = UIFont.systemFont(ofSize: 13, weight: .medium)
//        slider.maxLabelFont = UIFont.systemFont(ofSize: 13, weight: .medium)
//        slider.handleBorderColor = .black
//        slider.labelPadding = 2.0
//        slider.lineHeight = 2.0
//        slider.minLabelColor = .black
//        slider.maxLabelColor = .black
//        slider.handleBorderColor = .black
//        slider.numberFormatter.locale = Locale(identifier: "pl_PL")
//        slider.numberFormatter.numberStyle = NumberFormatter.Style.decimal
//        slider.step = 1
//        slider.enableStep = true
//        slider.selectedHandleDiameterMultiplier = 1.2
//        slider.minDistance = 1
//        slider.layer.addShadow()
//        return slider
//    }()
    
    let rangeSlider: RangeSeekSlider = {
        let slider = RangeSeekSlider()
        
        //xslider.disableRange = true
        slider.handleColor = .white
        slider.layer.addShadow()
        slider.handleDiameter = 28.0
        slider.lineHeight = 2.0
        slider.tintColor = UIColor(r: 231, g: 232, b: 240, a: 1)
        slider.colorBetweenHandles = UIColor(r: 56, g: 204, b: 102, a: 1)
        slider.minLabelFont = UIFont.systemFont(ofSize: 13, weight: .medium)
        slider.maxLabelFont = UIFont.systemFont(ofSize: 13, weight: .medium)
        slider.minLabelColor = .black
        slider.maxLabelColor = .black
        slider.numberFormatter.locale = Locale(identifier: "pl_PL")
        slider.numberFormatter.numberStyle = NumberFormatter.Style.decimal
        slider.step = 1
        slider.enableStep = true
        slider.selectedHandleDiameterMultiplier = 1.2
        slider.minDistance = 1
        
        slider.minValue = 0.0
        slider.maxValue = 100.0
        slider.selectedMinValue = 0.0
        
        return slider
    }()
    
    func setupViews(){
        addSubview(rangeSlider)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: rangeSlider)
        addConstraintsWithFormat(format: "V:|[v0]|", views: rangeSlider)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

