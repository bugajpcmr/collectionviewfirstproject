//
//  ChangeSortCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 01/08/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class ChangeSortCell: UICollectionViewCell {
    
    var myCollectionViewController: FilterViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    var filterButton: UIButton = {
        var button = UIButton()
        button.setTitleColor(.black, for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 15;
        button.setTitle("NAJBLIŻEJ",for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 13)
        button.backgroundColor = .white
        return button
    }()
    
    func setupViews(){
        addSubview(filterButton)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: filterButton)
        addConstraintsWithFormat(format: "V:|-20-[v0]-20-|", views: filterButton)
        
        filterButton.addTarget(self, action: #selector(handleBackgroundChange), for: .touchUpInside)
    }
    
    @objc func handleBackgroundChange(){
        myCollectionViewController?.tapFilterButton(cell: self)
        if filterButton.layer.backgroundColor == UIColor.customHardGray.cgColor {
            filterButton.layer.backgroundColor = UIColor.white.cgColor
            
        }else
        {
            filterButton.layer.backgroundColor = UIColor.customHardGray.cgColor
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
