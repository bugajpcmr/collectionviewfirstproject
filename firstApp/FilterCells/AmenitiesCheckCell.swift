//
//  AmenitiesCheckCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 01/08/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class AmenitiesCheckCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let checkBox: UIButton = {
        let box = UIButton()
        box.layer.masksToBounds = true
        box.layer.cornerRadius = 6
        box.backgroundColor = .white
        box.layer.borderColor = UIColor.customBoxGray.cgColor
        box.layer.borderWidth = 1.0
        return box
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 12)
        return label
    }()
    
    func setupViews(){
        addSubview(titleLabel)
        addSubview(checkBox)
        
        addConstraintsWithFormat(format: "V:|[v0]|", views: titleLabel)
        
        addConstraintsWithFormat(format: "H:|-16-[v0(18)]-10-[v1]-16-|", views: checkBox, titleLabel)
        addConstraintsWithFormat(format: "V:[v0(18)]", views: checkBox)
        checkBox.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        checkBox.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    @objc func buttonClicked(sender : UIButton){
        if checkBox.layer.backgroundColor == UIColor.customOrange.cgColor {
            checkBox.layer.backgroundColor = UIColor.white.cgColor

        }else
        {
            checkBox.layer.backgroundColor = UIColor.customOrange.cgColor
        }
    }
    

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

