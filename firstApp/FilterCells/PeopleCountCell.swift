//
//  PeopleCountCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 01/08/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class PeopleCountCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    let howManyBar: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "ilość osób"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    
    func setupViews(){
        addSubview(howManyBar)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: howManyBar)
        addConstraintsWithFormat(format: "V:|-20-[v0(36)]-20-|", views: howManyBar)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

