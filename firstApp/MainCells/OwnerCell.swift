//
//  OwnerCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

//MARK: - Owner cell
class OwnerCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    func setupViews(){
        addSubview(titleLabel)
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: titleLabel)
        addConstraintsWithFormat(format: "V:|[v0]|", views: titleLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
