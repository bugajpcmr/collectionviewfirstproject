//
//  ElementsCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 16/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class HorizontalCell: UICollectionViewCell,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCollectionView()
        setupViews()
    }
    
    let elementsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    let iconNamesArray = ["wifi","kettle","freezer","frigde","kitchenette","tv_room","smoke_area","washer","wc","wc_room"]
    let cellId = "cellId"
    
    private func setupCollectionView(){
        elementsCollectionView.dataSource = self
        elementsCollectionView.delegate = self
        elementsCollectionView.register(ElementCell.self, forCellWithReuseIdentifier: cellId)
        elementsCollectionView.isUserInteractionEnabled = false
    }
    
    let imageIconArrow: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "navigation_circle")
        imager.contentMode = ContentMode.scaleAspectFit
        
        return imager
    }()
    
    
    func setupViews(){
        //backgroundColor = .black
        addSubview(elementsCollectionView)
        addSubview(imageIconArrow)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: elementsCollectionView)
        addConstraintsWithFormat(format: "V:|-16-[v0]-16-|", views: elementsCollectionView)
        
        addConstraintsWithFormat(format: "H:[v0(26)]-16-|", views: imageIconArrow)
        addConstraintsWithFormat(format: "V:|-24-[v0(26)]", views: imageIconArrow)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if iconNamesArray.count < 6 {
//            return iconNamesArray.count
//        }
//        else{
//            return 6
//        }
        return iconNamesArray.count < 6 ? iconNamesArray.count:6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ElementCell
        cell.imageIcon.image = UIImage(named: iconNamesArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40, height: frame.height)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ElementCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageIcon: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "navigation_circle")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    
    
    func setupViews(){
        //backgroundColor = .green
        addSubview(imageIcon)
        addConstraintsWithFormat(format: "H:|[v0(40)]|", views: imageIcon)
        addConstraintsWithFormat(format: "V:|-16-[v0(40)]-20-|", views: imageIcon)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

