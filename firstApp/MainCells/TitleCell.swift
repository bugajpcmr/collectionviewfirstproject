//
//  TitleCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

//MARK: - Title cell
class TitleCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .boldSystemFont(ofSize: 16)
        return label
    }()
    
    let breakView: UIView = {
        let br = UIView()
        br.backgroundColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)
        return br
    }()
    
    func setupViews(){
        addSubview(titleLabel)
        addSubview(breakView)
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: titleLabel)
        addConstraintsWithFormat(format: "V:|[v0]|", views: titleLabel)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: breakView)
        addConstraintsWithFormat(format: "V:[v0(1)]|", views: breakView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
