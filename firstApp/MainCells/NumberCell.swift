//
//  NumberCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

//MARK: - Contact
class NumberCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let phoneNumberOneLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    let phoneNumberTwoLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    let iconImage: UIImageView = {
        let imager = UIImageView()
        //imager.image = UIImage(named: "object_1")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    func setupViews(){
        addSubview(phoneNumberOneLabel)
        addSubview(phoneNumberTwoLabel)
        addSubview(iconImage)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: phoneNumberOneLabel)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: phoneNumberTwoLabel)
        
        addConstraintsWithFormat(format: "V:|-20-[v0]-10-[v1]", views: phoneNumberOneLabel,phoneNumberTwoLabel)
        
        addConstraintsWithFormat(format: "H:[v0]-12-|", views: iconImage)
        addConstraintsWithFormat(format: "V:|-16-[v0]", views: iconImage)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
