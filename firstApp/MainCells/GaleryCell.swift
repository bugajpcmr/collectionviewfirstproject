//
//  GaleryCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

//MARK: - Galery similar objects
class GaleryCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageView: UIImageView = {
        let imager = UIImageView()
        imager.contentMode = ContentMode.scaleAspectFill
        imager.clipsToBounds = true
        imager.layer.masksToBounds = true
        imager.layer.cornerRadius = 6
        return imager
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 12)
        return label
    }()
    
    func setupViews(){
        addSubview(imageView)
        imageView.addSubview(titleLabel)
        
        addConstraintsWithFormat(format: "H:|[v0]|", views: imageView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: imageView)
        
        addConstraintsWithFormat(format: "H:|-38-[v0]|", views: titleLabel)
        addConstraintsWithFormat(format: "V:[v0]-16-|", views: titleLabel)
        
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0).cgColor,
                        UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.8).cgColor]
        layer.startPoint = CGPoint(x: 0.5,y: 0.6)
        layer.endPoint = CGPoint(x: 0.5, y: 0.95)
        imageView.layer.insertSublayer(layer, at: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
