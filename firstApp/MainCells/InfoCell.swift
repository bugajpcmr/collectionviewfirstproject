//
//  InfoCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

//MARK: - Long description
class InfoCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.textColor = .black
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 15)
        label.textAlignment = .left
        return label
    }()
    
    //    @objc func someAction(_ sender:UITapGestureRecognizer){
    //        print("zmiana")
    //    }
    
    func setupViews(){
        addSubview(titleLabel)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: titleLabel)
        addConstraintsWithFormat(format: "V:|-15-[v0]-20-|", views: titleLabel)
        //        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        //        self.addGestureRecognizer(gesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

