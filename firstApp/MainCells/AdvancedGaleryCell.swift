//
//  AdvancedGaleryCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 25/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class AdvancedGaleryCell: UICollectionViewCell {
    
    var myCollectionViewController: ObjectListViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageView: UIImageView = {
        let imager = UIImageView()
        imager.contentMode = ContentMode.scaleAspectFill
        imager.clipsToBounds = true
        imager.layer.masksToBounds = true
        imager.layer.cornerRadius = 6
        return imager
    }()
    
    let iconHeartImage: UIButton = {
        let imager = UIButton()
        imager.contentMode = ContentMode.scaleAspectFill
        return imager
    }()
    
    let imageTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 12)
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "SF Pro Display", size: 15)
        label.font = .boldSystemFont(ofSize: 15)
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 56/255, green: 204/255, blue: 102/255, alpha: 1)
        label.font = UIFont(name: "SF Pro Display", size: 15)
        label.font = .boldSystemFont(ofSize: 15)
        return label
    }()
    
    func setupViews(){
        addSubview(imageView)
        addSubview(iconHeartImage)
        imageView.addSubview(imageTitleLabel)
        addSubview(titleLabel)
        addSubview(priceLabel)

        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: imageView)
        addConstraintsWithFormat(format: "V:|-20-[v0(185)]", views: imageView)
        
        addConstraintsWithFormat(format: "H:|-38-[v0]|", views: imageTitleLabel)
        addConstraintsWithFormat(format: "V:[v0]-16-|", views: imageTitleLabel)
        
        addConstraintsWithFormat(format: "H:[v0(40)]-32-|", views: iconHeartImage)
        addConstraintsWithFormat(format: "V:|-36-[v0(40)]", views: iconHeartImage)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: titleLabel)
        addConstraintsWithFormat(format: "V:[v0]-16-[v1]", views: imageView, titleLabel)
        
        addConstraintsWithFormat(format: "H:[v0]-16-|", views: priceLabel)
        addConstraintsWithFormat(format: "V:[v0]-16-[v1]", views: imageView, priceLabel)

        iconHeartImage.addTarget(self, action: #selector(handleHeartIconAddition), for: .touchUpInside)

        
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0).cgColor,
                        UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.8).cgColor]
        layer.startPoint = CGPoint(x: 0.5,y: 0.5)
        layer.endPoint = CGPoint(x: 0.5, y: 0.8)
        imageView.layer.insertSublayer(layer, at: 0)
    }
    
    @objc func handleHeartIconAddition(){
        myCollectionViewController?.tapFavouriteIcon(cell: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
