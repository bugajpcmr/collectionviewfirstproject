//
//  StretchyHeaderCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

//MARK: - Headers
class StretchyHeaderCell: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)
        label.font = .systemFont(ofSize: 12)
        return label
    }()
    
    let breakViewUp: UIView = {
        let br = UIView()
        br.backgroundColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)
        return br
    }()
    
    let breakViewDown: UIView = {
        let br = UIView()
        br.backgroundColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)
        return br
    }()
    
    func setupViews(){
        addSubview(titleLabel)
        addSubview(breakViewUp)
        addSubview(breakViewDown)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: titleLabel)
        addConstraintsWithFormat(format: "V:|-10-[v0]-10-|", views: titleLabel)
        addConstraintsWithFormat(format: "H:|[v0]|", views: breakViewDown)
        addConstraintsWithFormat(format: "V:[v0(0.5)]|", views: breakViewDown)
        addConstraintsWithFormat(format: "H:|[v0]|", views: breakViewUp)
        addConstraintsWithFormat(format: "V:|[v0(0.5)]", views: breakViewUp)
        backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
