//
//  FirstCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

//MARK: - starting photo/image
class FirstCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageLabel: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "object_1")
        //imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    let iconHeartImage: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "navigation_circle")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    let iconWeightImage: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "navigation_circle")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.backgroundColor = UIColor(red: 56/255, green: 204/255, blue: 102/255, alpha: 1)
        label.textAlignment = .center;
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 18;
        label.font = UIFont(name: "SF Pro Display", size: 14)
        label.font = .boldSystemFont(ofSize: 12)
        return label
    }()
    
    func setupViews(){
        addSubview(imageLabel)
        imageLabel.addSubview(titleLabel)
        imageLabel.addSubview(iconHeartImage)
        imageLabel.addSubview(iconWeightImage)
        
        addConstraintsWithFormat(format: "H:|[v0]|", views: imageLabel)
        addConstraintsWithFormat(format: "V:|[v0]|", views: imageLabel)
        
        addConstraintsWithFormat(format: "H:|-16-[v0(138)]|", views: titleLabel)
        addConstraintsWithFormat(format: "V:[v0(35)]-24-|", views: titleLabel)
        
        addConstraintsWithFormat(format: "H:[v0(36)]-16-|", views: iconHeartImage)
        addConstraintsWithFormat(format: "V:|-16-[v0(36)]", views: iconHeartImage)
        
        addConstraintsWithFormat(format: "H:[v0(36)]-68-|", views: iconWeightImage)
        addConstraintsWithFormat(format: "V:|-16-[v0(36)]", views: iconWeightImage)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
