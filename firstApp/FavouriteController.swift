//
//  FavouriteController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 29/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class FavouriteController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let advancedGaleryCellId = "advancedgalerycell"
    let photos = ["room_1", "room_2", "object_2", "object_1", "room_1"]
    
    var FavouriteObjectsArray = [Object]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        registerCells()
        ObjectManager.getObjects()
        getFavourites()
        NotificationCenter.default.addObserver(self, selector: #selector(getFavourites), name: NSNotification.Name(rawValue: "reloadArray"), object: nil)
    }
    
    private func setupCollectionView(){
        navigationItem.title = "Ulubione"
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 16, right: 0)
        navigationItem.backBarButtonItem = .none
    }
    
    @objc private func getFavourites() {
        FavouriteObjectsArray.removeAll()
        for object in ObjectsArray {
            if object.isFeatured == 1 {
                FavouriteObjectsArray.append(object)
            }
        }
        collectionView.reloadData()
    }
    
    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(AdvancedGaleryCell.self, forCellWithReuseIdentifier: advancedGaleryCellId)
    }
    
    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: advancedGaleryCellId, for: indexPath) as! AdvancedGaleryCell
            cell.imageView.sd_setImage(with: URL(string: FavouriteObjectsArray[indexPath.row].url), placeholderImage: UIImage(named: "room_1"))
            cell.imageTitleLabel.text = "\(FavouriteObjectsArray[indexPath.row].driveTime) minut drogi"
            cell.iconHeartImage.setImage(UIImage(named: "heart_solid"), for: .normal)
            cell.titleLabel.text = FavouriteObjectsArray[indexPath.row].title
            cell.priceLabel.text = "\(FavouriteObjectsArray[indexPath.row].price) zł/os"
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return FavouriteObjectsArray.count
        default:
            return 1
        }
    }
    
    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width, height: 245)
        default:
            return CGSize(width: view.frame.width, height: 245)
        }
    }
    
    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}



