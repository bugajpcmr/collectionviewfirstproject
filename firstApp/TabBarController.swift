//
//  TabBarController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 24/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: MyTabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .gray
        
        let objectsController = CollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        let navigationController = UINavigationController(rootViewController: objectsController)
        navigationController.title = "Obiekty"
        navigationController.tabBarItem.image = UIImage(named: "objects_bar_black")
        
        let mainController = DataViewController()
//        let mainController = ObjectListViewController(collectionViewLayout: UICollectionViewFlowLayout())
        let secondNavigationController = UINavigationController(rootViewController: mainController)
        mainController.navigationItem.title = "Szukaj"
        secondNavigationController.title = "Szukaj"
        secondNavigationController.tabBarItem.image = UIImage(named: "search_bar_black")
        
        let compareVC = CompareCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        let compareNavigationController = UINavigationController(rootViewController: compareVC)
        compareVC.navigationItem.title = "Porównaj"
        compareNavigationController.title = "Porównaj"
        compareNavigationController.tabBarItem.image = UIImage(named: "weight_bar_black")
        
        let favouriteVC = FavouriteController(collectionViewLayout: UICollectionViewFlowLayout())
        let favouriteNavigationController = UINavigationController(rootViewController: favouriteVC)
        favouriteVC.navigationItem.title = "Ulubione"
        favouriteNavigationController.title = "Ulubione"
        favouriteNavigationController.tabBarItem.image = UIImage(named: "heart_bar_black")
        
        viewControllers = [navigationController, secondNavigationController, compareNavigationController, favouriteNavigationController]
        
        tabBar.isTranslucent = false
        tabBar.tintColor = .orange
        tabBar.unselectedItemTintColor = .black
        
    }
}
