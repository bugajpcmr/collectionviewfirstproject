//
//  MainPageController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 23/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class MainPageController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        let label = UILabel()
        let attributedString = NSMutableAttributedString(string: "Znajdź nocleg blisko miejsca pracy")
        let paragraphStyle = NSMutableParagraphStyle()
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        label.font = .systemFont(ofSize: 16)
        
        let FSimage = UIImageView()
        FSimage.image = UIImage(named: "logo_fs")
        
        let searchBar = UISearchBar()
        searchBar.barTintColor = UIColor(red: 142, green: 142, blue: 147, alpha: 0.24)
        searchBar.tintColor = .red
        searchBar.backgroundColor = UIColor(red: 142, green: 142, blue: 147, alpha: 0.24)
        searchBar.searchBarStyle = .prominent
        searchBar.placeholder = "Adres Twojej pracy"
        
        view.addSubview(label)
        view.addSubview(FSimage)
        view.addSubview(searchBar)

        view.addConstraintsWithFormat(format: "V:|-150-[v0]-24-[v1]", views: FSimage, label)
        view.addConstraintsWithFormat(format: "H:|-54-[v0]-53-|", views: label)
        view.addConstraintsWithFormat(format: "H:|-\((view.frame.width/2) - 88)-[v0(177)]-\((view.frame.width/2)-88)-|", views: FSimage)
        view.addConstraintsWithFormat(format: "V:|-190-[v0(50)]", views: FSimage)
        
        view.addConstraintsWithFormat(format: "V:[v0]-65-[v1(36)]", views: label, searchBar)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: searchBar)
    }
}

