import UIKit

class CollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        registerCells()
    }
    
    private func setupCollectionView(){
        navigationItem.title = "test"
        navigationItem.backBarButtonItem = .none
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 16, right: 0)
    }
    
    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(ClassCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ClassCell
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return 1
        }
    }
    
    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width, height: 50)
        default:
            return CGSize(width: view.frame.width, height: 50)
        }
    }
    
    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

class _TabBarController: MyTabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        let firstController = FirstController(collectionViewLayout: UICollectionViewFlowLayout())
        let navigationController = UINavigationController(rootViewController: firstController)
        navigationController.title = "Obiekty"
        navigationController.tabBarItem.image = UIImage(named: "objects_bar_black")
        
        //        let mainController = DataViewController()
        let secondController = SecondController(collectionViewLayout: UICollectionViewFlowLayout())
        let secondNavigationController = UINavigationController(rootViewController: mainController)
        secondController.navigationItem.title = "Szukaj"
        secondNavigationController.title = "Szukaj"
        secondNavigationController.tabBarItem.image = UIImage(named: "search_bar_black")
        
        viewControllers = [navigationController, secondNavigationController]
        
        tabBar.isTranslucent = false
        tabBar.tintColor = .orange
        tabBar.unselectedItemTintColor = .black
        
    }
}


class HorizontalCell: UICollectionViewCell,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCollectionView()
        setupViews()
    }
    
    let elementsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    let cellId = "cellId"
    
    private func setupCollectionView(){
        elementsCollectionView.dataSource = self
        elementsCollectionView.delegate = self
        elementsCollectionView.register(ElementCell.self, forCellWithReuseIdentifier: cellId)
        elementsCollectionView.isUserInteractionEnabled = false
    }

    func setupViews(){
        addSubview(elementsCollectionView)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: elementsCollectionView)
        addConstraintsWithFormat(format: "V:|-16-[v0]-16-|", views: elementsCollectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ElementCell
        cell.imageIcon.image = UIImage(named: "photo")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40, height: frame.height)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ElementCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageIcon: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "photo")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    func setupViews(){
        addSubview(imageIcon)
        addConstraintsWithFormat(format: "H:|[v0]|", views: imageIcon)
        addConstraintsWithFormat(format: "V:|[v0]|", views: imageIcon)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//dodać do 
override func viewDidLayoutSubviews() {
    addGradientToShowResultsButton()
}

private func addGradientToShowResultsButton(){
    let layer = CAGradientLayer()
    layer.frame = view.bounds
    layer.colors = [UIColor.yellow.cgColor,
                    UIColor.green.cgColor]
    layer.startPoint = CGPoint(x: 0.5,y: 0.5)
    layer.endPoint = CGPoint(x: 0.3, y: 0.3)
    showObjectsButton.layer.insertSublayer(layer, at: 0)
}

//MARK: - Button footer
private func addButtonView(){
    let v = UIView()
    v.layer.shadowColor = UIColor.black.cgColor
    v.layer.shadowOpacity = 1
    v.layer.shadowOffset = .zero
    v.layer.shadowRadius = 2
    v.layer.shadowPath = UIBezierPath(rect: v.bounds).cgPath
    v.backgroundColor = .black
    view.addSubview(v)
    v.addSubview(showObjectsButton)
    
    view.addConstraintsWithFormat(format: "H:|[v0]|", views: v)
    view.addConstraintsWithFormat(format: "V:[v0(60)]|", views: v)
    v.addConstraintsWithFormat(format: "H:|[v0]|", views: showObjectsButton)
    v.addConstraintsWithFormat(format: "V:|[v0]|", views: showObjectsButton)
    
    showObjectsButton.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
}

let showObjectsButton: UIButton = {
    let button = UIButton()
    button.setTitleColor(.black, for: .normal)
    button.titleLabel?.font = .boldSystemFont(ofSize: 22)
    button.setTitle("POKAŻ OBIEKTY",for: .normal)
    //button.titleLabel?.textColor = UIColor.white
    return button
}()

@objc func buttonClicked(sender : UIButton){
    let desVC = ObjectListViewController(collectionViewLayout: UICollectionViewFlowLayout())
    
    navigationController?.pushViewController(desVC, animated: true)
}
