//
//  ExtendedHorizontalViewController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 22/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

struct IconAndTitle {
    let iconName:String
    let title:String
}


class ExtendedHorizontalViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let iconsAndTitlesArray = [
    IconAndTitle(iconName: "wifi", title: "wifi"),
    IconAndTitle(iconName: "kettle", title: "czajnik elektryczny"),
    IconAndTitle(iconName: "frigde", title: "lodówka"),
    IconAndTitle(iconName: "tv_room", title: "telewizor w pokoju"),
    IconAndTitle(iconName: "smoke_area", title: "miejsce dla palących"),
    IconAndTitle(iconName: "freezer", title: "zamrażarka"),
    IconAndTitle(iconName: "kitchenette", title: "aneks kuchenny"),
    IconAndTitle(iconName: "wc", title: "łazienka w pokoju"),
    IconAndTitle(iconName: "wc_room", title: "parking"),
    IconAndTitle(iconName: "balcony", title: "balkon/taras"),
    IconAndTitle(iconName: "bathroom", title: "łazienka wspólna"),
    IconAndTitle(iconName: "wc_room", title: "wc w pokoju"),
    IconAndTitle(iconName: "wc", title: "wc wspólne"),
    IconAndTitle(iconName: "parking_truck", title: "parking ciężarowe"),
    IconAndTitle(iconName: "oven", title: "piekarnik"),
    IconAndTitle(iconName: "microwave", title: "kuchenka mikrofalowa"),
    IconAndTitle(iconName: "restaurant", title: "restauracja"),
    IconAndTitle(iconName: "washer", title: "pralka")]
    let mainCellId = "maincell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        navigationItem.title = "Udogodnienia"
        collectionView.backgroundColor = .white
        self.navigationController?.navigationBar.tintColor = .black

    }
    
    private func registerCells(){
        collectionView.register(MainCell.self, forCellWithReuseIdentifier: mainCellId)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconsAndTitlesArray.count
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainCellId, for: indexPath) as! MainCell
        cell.imageIcon.image = UIImage(named: iconsAndTitlesArray[indexPath.row].iconName)
        cell.titleLabel.text = iconsAndTitlesArray[indexPath.row].title
        if indexPath.row == iconsAndTitlesArray.count-1 {
            cell.breakView.backgroundColor = .white
        }
//        let currentItem = iconsAndTitlesArray[indexPath.row]
//        cell.imageIcon.image = UIImage(named: currentItem.iconName)
//        cell.titleLabel.text = currentItem.title
        
        return cell
    }
}

class MainCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    let imageIcon: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "navigation_circle")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    let breakView: UIView = {
        let br = UIView()
        br.backgroundColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)
        return br
    }()
    
    func setupViews(){
        
        addSubview(titleLabel)
        addSubview(imageIcon)
        addSubview(breakView)


        addConstraintsWithFormat(format: "V:|[v0]|", views: titleLabel)

        addConstraintsWithFormat(format: "H:|-6-[v0]-8-[v1]", views: imageIcon, titleLabel)
        addConstraintsWithFormat(format: "V:|[v0]|", views: imageIcon)
        
        addConstraintsWithFormat(format: "H:|-50-[v0]|", views: breakView)
        addConstraintsWithFormat(format: "V:[v0(1)]|", views: breakView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
