//
//  CollectionViewController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 10/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
//import Alamofire
import UIKit

struct ObjectCompare {
    let photos: [String]
    let title: String?
    let priceMax: String?
    let adres: [String]
    let capacityMax: String?
    let amenities: [IconAndTitle]
    let description: String?
}

class CompareCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let headerCellId = "headercell"
    let topPhotoCellId = "topphotocell"
    let priceMaxCellId = "pricemaxcell"
    let singleAdressCellId = "singleadresscell"
    let capacityMaxCellId = "capacitymaxcell"
    let amenitiesCellId = "amenitiescell"
    let aboutCellId = "aboutcell"
    let roomsCellId = "roomscell"
    let emptyObjectCellId = "emptyobjectcell"
    
    let headerNamesArray = ["", "CENA MAX", "ADRES", "MAX OSÓB", "UDOGODNIENIA", "OPIS", "POKOJE"]
    
    let photosArray = ["room_1", "room_2", "object_2", "object_1", "google"]
    let photosArrayTwo = ["room_1", "room_2", "object_2"]
    let compareTitle = "Noclegi Pracownicze Złotniki"
    let iconsArray = ["navigation_circle", "phone_circle", "close_x_comparison"]
    let comparePrice = "od 35 zł/os"
    let adressArray = ["ul. Myślęcin 16F", "Elbląg", "woj. Łódzkie"]
    let capacity = "Pomieści w sumie 30 osób"
    let iconsAndTitles = [
        IconAndTitle(iconName: "wifi", title: "wifi"),
        IconAndTitle(iconName: "kettle", title: "czajnik elektryczny"),
        IconAndTitle(iconName: "frigde", title: "lodówka"),
        IconAndTitle(iconName: "tv_room", title: "telewizor w pokoju"),
        IconAndTitle(iconName: "smoke_area", title: "miejsce dla palących"),
        IconAndTitle(iconName: "freezer", title: "zamrażarka"),
        IconAndTitle(iconName: "kitchenette", title: "aneks kuchenny"),
        IconAndTitle(iconName: "wc", title: "łazienka w pokoju"),
        IconAndTitle(iconName: "wc_room", title: "parking"),
        IconAndTitle(iconName: "balcony", title: "balkon/taras"),
        IconAndTitle(iconName: "bathroom", title: "łazienka wspólna"),
        IconAndTitle(iconName: "wc_room", title: "wc w pokoju"),
        IconAndTitle(iconName: "wc", title: "wc wspólne"),
        IconAndTitle(iconName: "parking_truck", title: "parking ciężarowe"),
        IconAndTitle(iconName: "oven", title: "piekarnik"),
        IconAndTitle(iconName: "microwave", title: "kuchenka mikrofalowa"),
        IconAndTitle(iconName: "restaurant", title: "restauracja"),
        IconAndTitle(iconName: "washer", title: "pralka")]
    let iconsAndTitlesRight = [
        IconAndTitle(iconName: "wifi", title: "wifi"),
        IconAndTitle(iconName: "kettle", title: "czajnik elektryczny"),
        IconAndTitle(iconName: "frigde", title: "lodówka"),
        IconAndTitle(iconName: "tv_room", title: "telewizor w pokoju"),
        IconAndTitle(iconName: "smoke_area", title: "miejsce dla palących"),
        IconAndTitle(iconName: "freezer", title: "zamrażarka"),
        IconAndTitle(iconName: "kitchenette", title: "aneks kuchenny"),
        IconAndTitle(iconName: "wc", title: "łazienka w pokoju"),
        IconAndTitle(iconName: "wc_room", title: "parking"),
        IconAndTitle(iconName: "balcony", title: "balkon/taras"),
        IconAndTitle(iconName: "bathroom", title: "łazienka wspólna"),
        IconAndTitle(iconName: "wc_room", title: "wc w pokoju"),
        IconAndTitle(iconName: "wc", title: "wc wspólne")]
    let compareDescription = "Wariacji na temat carpaccio znamy co najmniej kilka: carpaccio z łososia, z tuńczyka, z cielęciny czy carpaccio owocowe. Jedną z popularniejszych wersji tej przystawki jest carpaccio z wołowiny, które można zastąpić… w czasie wykwintnej kolacji we włoskim stylu. Wariacji na temat carpaccio znamy co najmniej kilka: carpaccio z łososia, z tuńczyka, z cielęciny czy carpaccio owocowe. Jedną z popularniejszych wersji tej przystawki jest carpaccio z wołowiny, które można zaserwować w czasie wykwintnej kolacji we włoskim stylu."
    
    let compareDescriptionRight = "Wariacji na temat carpaccio znamy co najmniej kilka: carpaccio z łososia, z tuńczyka, z cielęciny czy carpaccio owocowe. Jedną z popularniejszych wersji tej przystawki jest carpaccio z wołowiny, które można zastąpić… w czasie wykwintnej kolacji we włoskim stylu. Wariacji na temat carpacc"
    
    var rObject: ObjectCompare?
    var lObject: ObjectCompare?
    
    var aboutCellHeight: Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        registerCells()
        fetchData()
    }
    
    func fetchData(){
//        lObject = ObjectCompare(photos: photosArray, title: compareTitle, priceMax: comparePrice, adres: adressArray, capacityMax: capacity, amenities: iconsAndTitles, description: compareDescription)
//        rObject = ObjectCompare(photos: photosArrayTwo, title: compareTitle, priceMax: comparePrice, adres: adressArray, capacityMax: capacity, amenities: iconsAndTitlesRight, description: compareDescriptionRight)
        
        lObject = nil
        rObject = nil
        
        collectionView.reloadData()
    }
    
    func getAmenitiesCount() -> Int{
        return (lObject?.amenities.count ?? 0) >= (rObject?.amenities.count ?? 0) ?
            lObject?.amenities.count ?? 0 : rObject?.amenities.count ?? 0
    }
    
    func getPhotosCount() -> Int {
        return (lObject?.photos.count ?? 0) >= (rObject?.photos.count ?? 0)
            ? lObject?.photos.count ?? 0: rObject?.photos.count ?? 0
    }
    
    private func setupCollectionView(){
        navigationItem.title = "Porównanie"
        
        collectionView.backgroundColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = .black
        let br = UIView()
        br.backgroundColor = .red
        collectionView.addSubview(br)
        collectionView.addConstraintsWithFormat(format: "H:[v0(2)]", views: br)
        collectionView.addConstraintsWithFormat(format: "V:|[v0]|", views: br)
        //br.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
    }


    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(StretchyHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
        collectionView.register(TopPhotoCell.self, forCellWithReuseIdentifier: topPhotoCellId)
        collectionView.register(PriceMaxCell.self, forCellWithReuseIdentifier: priceMaxCellId)
        collectionView.register(SingleAdressCell.self, forCellWithReuseIdentifier: singleAdressCellId)
        collectionView.register(CapacityMaxCell.self, forCellWithReuseIdentifier: capacityMaxCellId)
        collectionView.register(AmenitiesCell.self, forCellWithReuseIdentifier: amenitiesCellId)
        collectionView.register(AboutCell.self, forCellWithReuseIdentifier: aboutCellId)
        collectionView.register(RoomsCell.self, forCellWithReuseIdentifier: roomsCellId)
        collectionView.register(EmptyObjectCell.self, forCellWithReuseIdentifier: emptyObjectCellId)
    }
    
    //MARK: - Secelcted Section/row
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if lObject == nil && rObject == nil {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emptyObjectCellId, for: indexPath) as! EmptyObjectCell
            cell.myCollectionViewController = self
            cell.titleLabel.text = "Dodaj Obiekt"
            //cell.iconAddObject.setImage(UIImage(named: iconsArray[0]), for: .normal)
            return cell
        }
        else if lObject != nil && rObject == nil {
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topPhotoCellId, for: indexPath) as! TopPhotoCell
                cell.myCollectionViewController = self
                    cell.titleLabel.text = lObject?.title ?? ""
                    cell.iconCancelImage.setImage(UIImage(named: iconsArray[2]), for: .normal)
                    cell.imageView.image = UIImage(named: lObject?.photos[0] ?? "")
                    return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emptyObjectCellId, for: indexPath) as! EmptyObjectCell
                cell.myCollectionViewController = self
                cell.titleLabel.text = "Dodaj Obiekt"
                //cell.iconAddObject.setImage(UIImage(named: iconsArray[0]), for: .normal)
                return cell
            }
        }else if lObject == nil && rObject != nil
        {
            if indexPath.row == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topPhotoCellId, for: indexPath) as! TopPhotoCell
                cell.myCollectionViewController = self
                cell.titleLabel.text = rObject?.title ?? ""
                cell.iconCancelImage.setImage(UIImage(named: iconsArray[2]), for: .normal)
                cell.imageView.image = UIImage(named: rObject?.photos[0] ?? "")
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emptyObjectCellId, for: indexPath) as! EmptyObjectCell
                cell.myCollectionViewController = self
                cell.titleLabel.text = "Dodaj Obiekt"
                //cell.iconAddObject.setImage(UIImage(named: iconsArray[0]), for: .normal)
                return cell
            }
        }
        else{
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topPhotoCellId, for: indexPath) as! TopPhotoCell
            cell.myCollectionViewController = self
            cell.iconCancelImage.setImage(UIImage(named: iconsArray[2]), for: .normal)
            if indexPath.row == 0 {
                cell.titleLabel.text = lObject?.title ?? ""
                cell.imageView.image = UIImage(named: lObject?.photos[0] ?? "")
                return cell
            }else{
                cell.titleLabel.text = rObject?.title ?? ""
                cell.imageView.image = UIImage(named: rObject?.photos[0] ?? "")
                return cell
            }
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: priceMaxCellId, for: indexPath) as! PriceMaxCell
            if indexPath.row == 0 {
                cell.priceLabel.text = lObject?.priceMax ?? ""
                return cell
            }
            else{
                cell.priceLabel.text = rObject?.priceMax ?? ""
                return cell
            }
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: singleAdressCellId, for: indexPath) as! SingleAdressCell
            if indexPath.row == 0 {
                cell.streetLabel.text = lObject?.adres[0] ?? ""
                cell.cityLabel.text = lObject?.adres[1] ?? ""
                cell.distrLabel.text = lObject?.adres[2] ?? ""
                return cell
            }
            else{
                cell.streetLabel.text = rObject?.adres[0] ?? ""
                cell.cityLabel.text = rObject?.adres[1] ?? ""
                cell.distrLabel.text = rObject?.adres[2] ?? ""
                return cell
            }
            
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: capacityMaxCellId, for: indexPath) as! CapacityMaxCell
            if indexPath.row == 0 {
                cell.capacityLabel.text = lObject?.capacityMax ?? ""
                return cell
            }
            else{
                cell.capacityLabel.text = rObject?.capacityMax ?? ""
                return cell
            }
            
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: amenitiesCellId, for: indexPath) as! AmenitiesCell
            
            cell.elementsCollectionView.reloadData()
        
            if indexPath.row == 0 {
                cell.iconsAndTitlesArray = lObject?.amenities ?? []
                return cell
            }
            else{
                cell.iconsAndTitlesArray = rObject?.amenities ?? []
                
                return cell
            }
            
        case 5:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: aboutCellId, for: indexPath) as! AboutCell
            if indexPath.row == 0 {
                cell.titleLabel.text = lObject?.description
                return cell
            }
            else{
                cell.titleLabel.text = rObject?.description
                return cell
            }
        case 6:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: roomsCellId, for: indexPath) as! RoomsCell
            cell.elementsCollectionView.reloadData()
            if indexPath.row == 0 {
                cell.photosArray = lObject?.photos ?? [""]
                return cell
            }
            else{
                cell.photosArray = rObject?.photos ?? [""]
                return cell
            }
            
        default:
                return UICollectionViewCell()
        }
        }
    }

    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if lObject != nil && rObject != nil {
            return 7
        }else{
            return 1
        }
    }

    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (view.frame.width/2.0) - 0.5
        
        switch indexPath.section {
        case 0:
            return CGSize(width: cellWidth, height: 212)
        case 1:
            return CGSize(width: cellWidth, height: 45)
        case 2:
            return CGSize(width: CGFloat(cellWidth), height: CGFloat(90))
        case 3:
            return CGSize(width: cellWidth, height: 45)
        case 4:
            return CGSize(width: cellWidth, height: CGFloat((getAmenitiesCount()*40)+10))
        case 5:
            let lHeight = Float(lObject?.description?.estimatedFrameHeight(textWidth: (cellWidth) - 32, textSize: 11, fontWeight: .regular, lineSpacing: 5) ?? 0) + 36.0
            
            let rHeight = Float(rObject?.description?.estimatedFrameHeight(textWidth: (cellWidth) - 32, textSize: 11, fontWeight: .regular, lineSpacing: 5) ?? 0) + 36.0
            
            aboutCellHeight = (lHeight > rHeight ? lHeight : rHeight)
            
            return CGSize(width: cellWidth, height: CGFloat(aboutCellHeight))
        case 6:
            return CGSize(width: cellWidth, height: CGFloat(getPhotosCount()*101)+16)
        default:
            return CGSize(width: cellWidth, height: 50)
        }

    }

    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    //MARK: - section HEADERS
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! StretchyHeaderCell
        header.titleLabel.text = headerNamesArray[indexPath.section]
        return header
    }
    
    //MARK: - Headers - height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case 0:
            return CGSize(width: view.frame.width, height: 0)
        default:
            return CGSize(width: view.frame.width, height: 50)
        }
    }
    
    func tapCancelIcon(cell: UICollectionViewCell){
        
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        
        if indexPath.row == 0  {
            lObject = nil
        }
        else {
            rObject = nil
        }
        
        collectionView.reloadData()
    }
    
    func tapAddIcon(cell: UICollectionViewCell){
        
        print("dodano obiekt")
        
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        
        if indexPath.row == 0  {
            lObject = ObjectCompare(photos: photosArray, title: "Obiekt pierwszy", priceMax: comparePrice, adres: adressArray, capacityMax: capacity, amenities: iconsAndTitles, description: compareDescription)
        }
        else {
            rObject = ObjectCompare(photos: photosArrayTwo, title: "Obiekt drugi", priceMax: comparePrice, adres: adressArray, capacityMax: capacity, amenities: iconsAndTitlesRight, description: compareDescriptionRight)
        }
        
        collectionView.reloadData()
    }
}
