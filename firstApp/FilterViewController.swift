//
//  FilterController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 01/08/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class FilterViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let headerCellId = "headercell"
    let changeSortCellId = "changesortcell"
    let peopleCountCellId = "peoplecountcell"
    let priceSliderCellId = "priceslidercell"
    let amenitiesCheckCellId = "amenitiescheckcell"
    
    let headerNamesArray = ["ZMIEŃ SORTOWANIE", "ILOŚĆ OSÓB", "CENA ZA OSOBĘ ZA DOBĘ", "UDOGODNIENIA"]
    let buttonNamesArray = ["NAJBLIŻEJ", "NAJTANIEJ", "NAJNOWSZE"]
    let amenitiesNamesArray = ["wifi","piekarnik","czajnik elektryczny","kuchenka mikrofalowa","lodówka","restauracja","łazienka w pokoju","parking ciężarowe","łazienka wspólna","pralka","wc w pokoju","miejsce dla palących","wc wspólne","balkon/taras"]
    //var isBoxTapped = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .black
        setupCollectionView()
        registerCells()
        addButtonView()
    }
    
    private func setupCollectionView(){
        navigationItem.title = "Filtry"
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 70, right: 0)
//        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(tapButton))
        
        let rightBarButton = UIBarButtonItem(title: "Wyczyść", style: .plain, target: self, action: #selector(tapButton))
        rightBarButton.tintColor = .red
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func tapButton(){
        print("TAP")
    }
    
    override func viewDidLayoutSubviews() {
        addGradientToShowResultsButton()
    }
    
    private func addGradientToShowResultsButton(){
        let layer = CAGradientLayer()
        layer.frame = view.bounds
        layer.colors = [UIColor(red: 254/255, green: 140/255, blue: 0/255, alpha: 1).cgColor,
                        UIColor(red: 248/255, green: 54/255, blue: 0/255, alpha: 1).cgColor]
        layer.startPoint = CGPoint(x: 0.0,y: 0.0)
        layer.endPoint = CGPoint(x: 0.3, y: 0.3)
        resultsButton.layer.insertSublayer(layer, at: 0)
        resultsButton.layer.addShadow()
    }
    
    //MARK: - Button footer
    private func addButtonView(){
        let v = UIView()
        v.layer.shadowColor = UIColor.black.cgColor
        v.layer.shadowOpacity = 1
        v.layer.shadowOffset = .zero
        v.layer.shadowRadius = 2
        v.layer.shadowPath = UIBezierPath(rect: v.bounds).cgPath
        v.backgroundColor = .white
        view.addSubview(v)
        v.addSubview(resultsButton)
        
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: v)
        view.addConstraintsWithFormat(format: "V:[v0(60)]|", views: v)
        v.addConstraintsWithFormat(format: "H:|[v0]|", views: resultsButton)
        v.addConstraintsWithFormat(format: "V:|[v0]|", views: resultsButton)
        
        resultsButton.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    @objc func buttonClicked(sender : UIButton){
        print("Show results")
    }
    
    let resultsButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 15)
        button.setTitle("POKAŻ WYNIKI",for: .normal)
        button.titleLabel?.textColor = UIColor.white
        return button
    }()
    
    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(StretchyHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
        collectionView.register(ChangeSortCell.self, forCellWithReuseIdentifier: changeSortCellId)
        collectionView.register(PeopleCountCell.self, forCellWithReuseIdentifier: peopleCountCellId)
        collectionView.register(PriceSliderCell.self, forCellWithReuseIdentifier: priceSliderCellId)
        collectionView.register(AmenitiesCheckCell.self, forCellWithReuseIdentifier: amenitiesCheckCellId)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: changeSortCellId, for: indexPath) as! ChangeSortCell
            cell.myCollectionViewController = self
            cell.filterButton.setTitle(buttonNamesArray[indexPath.row], for: .normal)
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: peopleCountCellId, for: indexPath) as! PeopleCountCell
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: priceSliderCellId, for: indexPath) as! PriceSliderCell
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: amenitiesCheckCellId, for: indexPath) as! AmenitiesCheckCell
            cell.titleLabel.text = amenitiesNamesArray[indexPath.row]
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    //View change
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath)
    }
    
    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 3:
            return amenitiesNamesArray.count
        default:
            return 1
        }
    }
    
    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width/3, height: 76)
        case 2:
            return CGSize(width: view.frame.width, height: 93)
        case 3:
            return CGSize(width: view.frame.width/2, height: 40)
        default:
            return CGSize(width: view.frame.width, height: 76)
        }
    }
    
    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - section HEADERS
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! StretchyHeaderCell
        header.titleLabel.text = headerNamesArray[indexPath.section]
        header.titleLabel.font = .systemFont(ofSize: 13)
        return header
    }
    
    //MARK: - Headers - height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 45)
    }
    
    func tapFilterButton(cell: UICollectionViewCell){
        guard let indexPath = collectionView.indexPath(for: cell) else { return }
        
        switch indexPath.row {
        case 0:
            print("NAJBLIŻEJ")
        case 1:
            print("NAJTANIEJ")
        case 2:
            print("NAJNOWSZE")
        default:
            print("NAJBLIŻEJ")
        }
        
        collectionView.reloadData()
    }
}

