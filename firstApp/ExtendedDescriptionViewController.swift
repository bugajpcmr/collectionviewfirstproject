//
//  ExtendedDescriptionView.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 22/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import UIKit

class ExtendedDescriptionViewController: UIViewController {
    
    var descriptionText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.navigationController?.navigationBar.tintColor = .black
        navigationItem.title = "O Obiekcie"
        setupView()
        }
    
    private func setupView() {
        let label = UILabel()
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 7
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        label.text = descriptionText
        view.addSubview(label)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: label)
        view.addConstraintsWithFormat(format: "V:|-110-[v0]", views: label)
    }
}

