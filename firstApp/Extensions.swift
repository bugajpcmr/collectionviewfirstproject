//
//  Extensions.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 09/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...){
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}

extension CALayer {
    func addShadow() {
        shadowColor = UIColor.black.cgColor
        shadowOpacity = 0.1
        shadowOffset = CGSize(width: -1, height: 3)
        shadowRadius = 5
    }
}

class PaddedTextField: UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 19, y: bounds.origin.y, width: bounds.width - 20, height: bounds.height)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 19, y: bounds.origin.y, width: bounds.width - 20, height: bounds.height)
    }
}

extension UIViewController {
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel()
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 14;
        toastLabel.clipsToBounds = true
        self.view.addSubview(toastLabel)
        self.view.addConstraintsWithFormat(format: "V:[v0(35)]-60-|", views: toastLabel)
        self.view.addConstraintsWithFormat(format: "H:[v0(260)]", views: toastLabel)
        toastLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        UIView.animate(withDuration: 1.0, delay: 2.5, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }

extension UIColor {
    convenience init(r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
    
    static var mainOrange = UIColor(r: 254, g: 117, b: 0, a: 1)
    static var customLightGray = UIColor(r: 109, g: 109, b: 114, a: 1)
    static var customHardGray = UIColor(r: 202, g: 206, b: 219, a: 1)
    static var customBoxGray = UIColor(r:202, g:206, b:219, a:1)
    static var customOrange = UIColor(r: 254, g: 140, b: 0, a: 1)
}

extension UIView {
    func setGradientBackground(colorOne: UIColor, colorTwo: UIColor){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
}

class MyTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
    }
}

extension MyTabBarController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let tabViewControllers = tabBarController.viewControllers, let toIndex = tabViewControllers.firstIndex(of: viewController) else {
            return false
        }
        animateToTab(toIndex: toIndex)
        return true
    }
    
    func animateToTab(toIndex: Int) {
        guard let tabViewControllers = viewControllers,
            let selectedVC = selectedViewController else { return }
        
        guard let fromView = selectedVC.view,
            let toView = tabViewControllers[toIndex].view,
            let fromIndex = tabViewControllers.firstIndex(of: selectedVC),
            fromIndex != toIndex else { return }
        
        
        // Add the toView to the tab bar view
        fromView.superview?.addSubview(toView)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width
        let scrollRight = toIndex > fromIndex
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView.center = CGPoint(x: fromView.center.x + offset, y: toView.center.y)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .curveEaseOut,
                       animations: {
                        // Slide the views by -offset
                        fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y)
                        toView.center = CGPoint(x: toView.center.x - offset, y: toView.center.y)
                        
        }, completion: { finished in
            // Remove the old view from the tabbar view.
            fromView.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }
}

//extension UIImageView {
//    func sdSetImage(urlString: String){
//        sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "placeholder.png"))
//        sd_setShowActivityIndicatorView(true)
//        sd_setIndicatorStyle(.gray)
//    }
//}
    
//    func showActivityIndicator(activityIndicator: UIActivityIndicatorView ){
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
//        activityIndicator.color = UIColor.mainOrange
//        activityIndicator.backgroundColor = UIColor(r: 0, g: 0, b: 0, a: 0.2)
//        addSubview(activityIndicator)
//        addConstraintsWithFormat(format: "H:|[v0]|", views: activityIndicator)
//        addConstraintsWithFormat(format: "V:|[v0]|", views: activityIndicator)
//    }
//}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func presentAlert(textForError: String?){
        let alertController = UIAlertController(title: "", message: textForError, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alertController.view.tintColor = UIColor.mainOrange
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(alertController, animated: true)
    }
}

extension String {
    func estimatedFrameHeight(textWidth: CGFloat, textSize: CGFloat, fontWeight: UIFont.Weight, lineSpacing: CGFloat) -> CGFloat {
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        style.lineSpacing = lineSpacing
        let size = CGSize(width: textWidth, height: 1000)
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: textSize, weight: fontWeight), NSAttributedString.Key.paragraphStyle: style]
        let estimatedFrame = NSString(string: self).boundingRect(with: size, options: .usesLineFragmentOrigin
            , attributes: attributes, context: nil)
        return estimatedFrame.height
    }
    
    func callToNumber(){
        let formatedNumber = components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        if let url = URL(string: "tel://\(formatedNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    func toDouble() -> Double {
        return Double(self) ?? 0
    }
}

//extension CALayer {
//    func addShadow() {
//        shadowColor = UIColor.black.cgColor
//        shadowOpacity = 0.1
//        shadowOffset = CGSize(width: -1, height: 3)
//        shadowRadius = 5
//    }
//}

//import UIKit
//
//
//var lFlat: ObjectExpanded?
//var rFlat: ObjectExpanded?
//
//class CompareViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
//
//
//    private let headerCellId = "headerId"
//    private let topCellId = "topCellId"
//    private let priceCellId = "priceCellId"
//    private let simpleTextCellId = "simpleTextCellId"
//    private let descTextCellID = "descTextCellId"
//    private let facilityCellId = "facilityCellId"
//    private let compareSmallPhotoCellId = "compareSmallPhotoCellId"
//    private let emptyCellId = "emptyCellId"
//    private let vertDividerWidth: CGFloat = 0.5
//
//    private let headerTitlesArray = ["", "CENA MIN", "ADRES", "MAX OSÓB", "UDOGODNIENIA", "OPIS", "POKOJE"]
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        setNavigationBars(titleName: "Porównanie", navigationController: navigationController, navigationItem: navigationItem)
//        setupCollectionViewController()
//        registerCells()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        collectionView.reloadData()
//    }
//
//    private func setupCollectionViewController(){
//        collectionView.backgroundColor = .white
//        collectionView.backgroundView = createBackgroundForCollectionView()
//    }
//
//    private func createBackgroundForCollectionView() -> UIView {
//        let backgroundCollectionView = UIView()
//        let grayDivider = UIView()
//        grayDivider.backgroundColor = UIColor(r: 200, g: 199, b: 204, a: 1)
//
//        backgroundCollectionView.addSubview(grayDivider)
//        backgroundCollectionView.addConstraintsWithFormat(format: "H:[v0(0.5)]", views: grayDivider)
//        backgroundCollectionView.addConstraintsWithFormat(format: "V:|[v0]|", views: grayDivider)
//        grayDivider.centerXAnchor.constraint(equalTo: backgroundCollectionView.centerXAnchor).isActive = true
//
//        return backgroundCollectionView
//    }
//
//    private func registerCells() {
//        collectionView.register(HeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
//        collectionView.register(CompareFlatTopCell.self, forCellWithReuseIdentifier: topCellId)
//        collectionView.register(CompareFlatPriceCell.self, forCellWithReuseIdentifier: priceCellId)
//        collectionView.register(SimpleTextLabelCell.self, forCellWithReuseIdentifier: simpleTextCellId)
//        collectionView.register(DescTextViewCell.self, forCellWithReuseIdentifier: descTextCellID)
//        collectionView.register(FacilityCell.self, forCellWithReuseIdentifier: facilityCellId)
//        collectionView.register(CompareRoomPhotoCell.self, forCellWithReuseIdentifier: compareSmallPhotoCellId)
//        collectionView.register(EmptyCell.self, forCellWithReuseIdentifier: emptyCellId)
//    }
//
//    // Methods for sections/headers.
//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return (rFlat == nil || lFlat == nil) ? 1:headerTitlesArray.count
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! HeaderCell
//        cell.titleLabel.text = headerTitlesArray[indexPath.section]
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return section == 0 ? CGSize(width: view.frame.width, height: 0):CGSize(width: view.frame.width, height: 44)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return vertDividerWidth
//    }
//
//    // Methods for items in sections.
//
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        switch indexPath.section {
//        case 0:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topCellId, for: indexPath) as! CompareFlatTopCell
//            cell.myCollectionView = self
//
//            if indexPath.row == 0 {
//
//                cell.xImageView.isHidden = lFlat == nil
//                cell.flatTitle.text = lFlat?.name ?? "Dodaj obiekt do porównania."
//                cell.addObjectButton.isHidden = lFlat != nil
//
//
//                if let unwrapedObject = lFlat {
//
//                    if !unwrapedObject.images.isEmpty {
//                        cell.noPhotoImage.isHidden = true
//                        cell.flatImageView.sdSetImage(urlString: unwrapedObject.images[0])
//                    }
//                    else {
//                        cell.noPhotoImage.isHidden = false
//                        cell.flatImageView.image = nil
//                        cell.flatImageView.backgroundColor = .mainLightGray
//                    }
//                }
//                else {
//                    cell.noPhotoImage.isHidden = true
//                    cell.flatImageView.image = nil
//                    cell.flatImageView.backgroundColor = .clear
//                }
//
//            } else if indexPath.row == 1 {
//
//                cell.xImageView.isHidden = rFlat == nil
//                cell.flatTitle.text = rFlat?.name ?? "Dodaj obiekt do porównania."
//                cell.addObjectButton.isHidden = rFlat != nil
//
//                if let unwrapedObject = rFlat {
//
//                    if !unwrapedObject.images.isEmpty {
//                        cell.noPhotoImage.isHidden = true
//                        cell.flatImageView.sdSetImage(urlString: unwrapedObject.images[0])
//                    }
//                    else {
//                        cell.noPhotoImage.isHidden = false
//                        cell.flatImageView.image = nil
//                        cell.flatImageView.backgroundColor = .mainLightGray
//                    }
//                }
//                else {
//                    cell.noPhotoImage.isHidden = true
//                    cell.flatImageView.image = nil
//                    cell.flatImageView.backgroundColor = .clear
//                }
//
//            }
//
//            return cell
//        case 1:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: priceCellId, for: indexPath) as! CompareFlatPriceCell
//
//            if indexPath.row == 0 {
//                cell.priceLabel.text = "od \(lFlat?.lowestPrice.description ?? "-") zł/os."
//            } else if indexPath.row == 1 {
//                cell.priceLabel.text = "od \(rFlat?.lowestPrice.description ?? "-") zł/os."
//            }
//
//            return cell
//        case 2:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: simpleTextCellId, for: indexPath) as! SimpleTextLabelCell
//
//            if indexPath.row == 0 {
//                cell.simpleTextLabel.text = "ul. \(lFlat?.street ?? "-"), \(lFlat?.city ?? "-"), woj. \(lFlat?.voivodeship  ?? "-")"
//            } else if indexPath.row == 1 {
//                cell.simpleTextLabel.text = "ul. \(rFlat?.street ?? "-"), \(rFlat?.city ?? "-"), woj. \(rFlat?.voivodeship ?? "-")"
//            }
//
//            return cell
//        case 3:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: simpleTextCellId, for: indexPath) as! SimpleTextLabelCell
//
//            if indexPath.row == 0 {
//                cell.simpleTextLabel.text = lFlat?.peopleCount.description ?? "-"
//            } else if indexPath.row == 1 {
//                cell.simpleTextLabel.text = rFlat?.peopleCount.description ?? "-"
//            }
//
//            return cell
//        case 4:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: facilityCellId, for: indexPath) as! FacilityCell
//
//            cell.enableSmallDescView()
//
//            let lFac = lFlat?.facilities
//            let rFac = rFlat?.facilities
//
//            let index = indexPath.row
//
//            if index % 2 == 0 && index / 2 < lFac?.count ?? 0 {
//                cell.facilityImageView.image = UIImage(named: lFac?[index / 2].macro ?? "-")
//                cell.descriptionLabel.text = lFac?[index / 2].name ?? "-"
//            } else if index % 2 == 1 && index / 2 < rFac?.count ?? 0 {
//                cell.facilityImageView.image = UIImage(named: rFac?[index / 2].macro ?? "-")
//                cell.descriptionLabel.text = rFac?[index / 2].name ?? "-"
//            } else {
//                cell.facilityImageView.isHidden = true
//                cell.descriptionLabel.isHidden = true
//            }
//
//            return cell
//        case 5:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: descTextCellID, for: indexPath) as! DescTextViewCell
//
//            if indexPath.row == 0 {
//                cell.descTextView.attributedText = NSAttributedString(string: lFlat?.description ?? "-" , attributes: DescTextViewCell.attributes)
//            } else if indexPath.row == 1 {
//                cell.descTextView.attributedText = NSAttributedString(string: rFlat?.description ?? "-", attributes: DescTextViewCell.attributes)
//            }
//            return cell
//        case 6:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: compareSmallPhotoCellId, for: indexPath) as! CompareRoomPhotoCell
//
//            let noOfCellsInColumn = 5
//
//            let lRoom = lFlat?.rooms
//            let rRoom = rFlat?.rooms
//
//            let index = indexPath.row
//
//            if index % 2 == 0 && index / 2 < lRoom?.count ?? 0 && index / 2 < noOfCellsInColumn {
//                cell.enableRoomImageView()
//
//                if !lRoom![index / 2].images.isEmpty {
//                    cell.noPhotoImage.isHidden = true
//                    cell.roomImageView.sdSetImage(urlString: lRoom?[index / 2].images[0] ?? "")
//                } else {
//                    cell.noPhotoImage.isHidden = false
//                    cell.roomImageView.image = nil
//                    cell.roomImageView.backgroundColor = .mainLightGray
//                }
//
//            }
//            else if index % 2 == 1 && index / 2 < rRoom?.count ?? 0 && index / 2 < noOfCellsInColumn {
//                cell.enableRoomImageView()
//
//                if !rRoom![index / 2].images.isEmpty {
//                    cell.noPhotoImage.isHidden = true
//                    cell.roomImageView.sdSetImage(urlString: rRoom?[index / 2].images[0] ?? "")
//                } else {
//                    cell.noPhotoImage.isHidden = false
//                    cell.roomImageView.image = nil
//                    cell.roomImageView.backgroundColor = .mainLightGray
//                }
//
//            }
//            else if index % 2 == 0 && index / 2 < lRoom?.count ?? 0 && index / 2 == noOfCellsInColumn + 1 {
//                let additionalNo = lRoom?.count ?? 0 - noOfCellsInColumn
//
//                cell.enableAdditionalNoView()
//                cell.additionalNoView.text = "+\(additionalNo)"
//            }
//            else if index % 2 == 1 && index / 2 < rRoom?.count ?? 0 && index / 2 == noOfCellsInColumn + 1 {
//                let additionalNo = rRoom?.count ?? 0 - noOfCellsInColumn
//                cell.enableAdditionalNoView()
//                cell.additionalNoView.text = "+\(additionalNo)"
//            }
//            else {
//                cell.noPhotoImage.isHidden = true
//                cell.roomImageView.isHidden = true
//                cell.additionalNoView.isHidden = true
//            }
//
//            return cell
//        default:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emptyCellId, for: indexPath) as! EmptyCell
//
//            return cell
//        }
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        switch section {
//        case 4:
//            return (lFlat?.facilities.count ?? 0 > rFlat?.facilities.count ?? 0 ? lFlat?.facilities.count ?? 0 : rFlat?.facilities.count ?? 0) * 2
//        case 6:
//            let calculatedNoOfCells = (lFlat?.rooms.count ?? 0 > rFlat?.rooms.count ?? 0 ? lFlat?.rooms.count ?? 0 : rFlat?.rooms.count ?? 0) * 2 + 2
//            let noOfCellsInColumn = 5
//            let maxNoOfCells = noOfCellsInColumn * 2 + 2
//            let numberOfItems = calculatedNoOfCells < maxNoOfCells ? calculatedNoOfCells : maxNoOfCells
//            return numberOfItems
//        default:
//            return 2
//        }
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let cellWidth = (view.frame.width - vertDividerWidth) / 2
//
//        switch indexPath.section {
//        case 0:
//            return CGSize(width: cellWidth, height: 204)
//        case 1:
//            return CGSize(width: cellWidth, height: 45)
//        case 2:
//            let lStr = "ul. \(lFlat?.street ?? "-"), \(lFlat?.city ?? "-"), woj. \(lFlat?.voivodeship ?? "-")"
//            let rStr = "ul. \(rFlat?.street ?? "-"), \(rFlat?.city ?? "-"), woj. \(rFlat?.voivodeship ?? "-")"
//
//            let lHeight = lStr.estimatedFrameHeight(textWidth: cellWidth - 32, textSize: 11, fontWeight: .regular, lineSpacing: 0)
//            let rHeight = rStr.estimatedFrameHeight(textWidth: cellWidth - 32, textSize: 11, fontWeight: .regular, lineSpacing: 0)
//
//            return CGSize(width: cellWidth, height: (lHeight > rHeight ? lHeight : rHeight) + 32)
//        case 3:
//            let lStr = lFlat?.peopleCount.description ?? "-"
//            let rStr = rFlat?.peopleCount.description ?? "-"
//
//            let lHeight = lStr.estimatedFrameHeight(textWidth: cellWidth - 32, textSize: 11, fontWeight: .regular, lineSpacing: 0)
//            let rHeight = rStr.estimatedFrameHeight(textWidth: cellWidth - 32, textSize: 11, fontWeight: .regular, lineSpacing: 0)
//
//            return CGSize(width: cellWidth, height: (lHeight > rHeight ? lHeight : rHeight) + 32)
//        case 4:
//            return CGSize(width: cellWidth, height: 50)
//        case 5:
//            // It measure maxLines. It should be 10 lines with +5 lineSpacing (20) or the height of this string. Graphic designer approved.
//            //            let maxHeightStr = "Wariacji na temat carpaccio znamy co najmniej kilka: carpaccio z łososia, z tuńczyka, z cielęciny czy carpaccio owocowe. Jedną z popularniejszych wersji tej przystawki jest carpaccio z wołowiny, które można zastąpić w czasie wykwintnej kolacji we włoskim stylu."
//
//            let lStr = lFlat?.description ?? "-"
//            let rStr = rFlat?.description ?? "-"
//
//            let lineSpc = 5 as CGFloat // DescTextViewCell.style.lineSpacing
//
//            //            let maxHeight = maxHeightStr.estimatedFrameHeight(textWidth: cellWidth - 32, textSize: 11, fontWeight: .regular, lineSpacing: lineSpc) + 32 + lineSpc
//            let maxHeight = CGFloat(214)
//
//
//            let lHeight = lStr.estimatedFrameHeight(textWidth: cellWidth - 32, textSize: 11, fontWeight: .regular, lineSpacing: lineSpc)
//            let rHeight = rStr.estimatedFrameHeight(textWidth: cellWidth - 32, textSize: 11, fontWeight: .regular, lineSpacing: lineSpc)
//
//            let measuredHeight = (lHeight > rHeight ? lHeight : rHeight) + 32
//
//            return CGSize(width: cellWidth, height: measuredHeight <= maxHeight ? measuredHeight : maxHeight)
//        case 6:
//            var cellWithPhotoHeight: CGFloat = 100
//            let cellWithNumberHeight: CGFloat = 85
//
//            //            let lRoom = lFlat.roomsImagesNames
//            //            let rRoom = rFlat.roomsImagesNames
//
//            let noOfCellsInColumn = 5
//
//            let index = indexPath.row
//
//            if index / 2 == 0 { cellWithPhotoHeight += 16 }
//
//            if index / 2 < noOfCellsInColumn {
//                return CGSize(width: cellWidth, height: cellWithPhotoHeight)
//            }
//            else if index / 2 == noOfCellsInColumn + 1 {
//                return CGSize(width: cellWidth, height: cellWithNumberHeight)
//            }
//            else {
//                return CGSize(width: cellWidth, height: 0)
//            }
//        default:
//            return CGSize(width: cellWidth, height: 0)
//        }
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        switch indexPath.section {
//        case 5:
//            let desVC = FlatDescriptionViewController()
//
//            if indexPath.row == 0 {
//                desVC.descriptionText = lFlat?.description ?? "-"
//            } else if indexPath.row == 1 {
//                desVC.descriptionText = rFlat?.description ?? "-"
//            }
//            navigationController?.pushViewController(desVC, animated: true)
//        case 6:
//
//            let layout = UICollectionViewFlowLayout()
//            layout.scrollDirection = .horizontal
//            let desVC = PhotosViewController(collectionViewLayout: layout)
//
//            if indexPath.row % 2 == 0 {
//                guard lFlat?.rooms.count ?? 0 >= indexPath.row else { return }
//                guard !(lFlat?.rooms[indexPath.row / 2].images.isEmpty ?? true) else { return }
//                desVC.photosArray = lFlat?.rooms[indexPath.row / 2].images
//            } else {
//                guard rFlat?.rooms.count ?? 0 > indexPath.row/2 else { return }
//                guard !(rFlat?.rooms[indexPath.row / 2].images.isEmpty ?? true) else { return }
//                desVC.photosArray = rFlat?.rooms[indexPath.row / 2].images
//            }
//            navigationController?.pushViewController(desVC, animated: true)
//        default:
//            break
//        }
//    }
//
//    func handleRemoveObject(cell: UICollectionViewCell){
//        if let indexPath = collectionView?.indexPath(for: cell) {
//
//            if indexPath.row == 0 {
//                lFlat = nil
//            }
//            else {
//                rFlat = nil
//            }
//            collectionView.reloadData()
//            setBadgeCompareTab()
//        }
//    }
//
//    func handleAddObjectButtonTap(){
//        self.tabBarController?.selectedIndex = 0
//    }
//
//}
