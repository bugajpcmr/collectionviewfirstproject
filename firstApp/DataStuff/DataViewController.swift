//
//  DataViewController.swift
//  twit
//
//  Created by Bartosz Bugajski on 12/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
import UIKit

class DataViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showAlertExists), name: NSNotification.Name(rawValue: "alreadyExists"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showVideoSavedToast), name: NSNotification.Name(rawValue: "objectSaved"), object: nil)
        
        setupViews()
        addButtonView()
        view.backgroundColor  = .mainOrange
        navigationItem.title = "Dodaj obiekt"
    }
    @objc func showVideoSavedToast(){
        self.showToast(message: "Zapisano nowy obiekt", font: .boldSystemFont(ofSize: 14))
    }
    
    @objc func showAlertExists(){
        let alert = UIAlertController(title: "Błąd", message: "Obiekt o podanym id lub nazwie już istnieje", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            @unknown default:
                print("error")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func setupViews(){
        view.addSubview(addButton)
        view.addSubview(idLabel)
        view.addSubview(titleLabel)
        view.addSubview(urlLabel)
        view.addSubview(isFeaturedLabel)
        view.addSubview(priceLabel)
        view.addSubview(driveTimeLabel)
        
        addButton.addTarget(self, action: #selector(handleAddButtonTap), for: .touchUpInside)
        
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: idLabel)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: titleLabel)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: urlLabel)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: isFeaturedLabel)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: priceLabel)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: driveTimeLabel)
        view.addConstraintsWithFormat(format: "H:|-32-[v0]-32-|", views: addButton)
        
        view.addConstraintsWithFormat(format: "V:|-60-[v0(40)]-20-[v1(40)]-20-[v2(40)]-20-[v3(40)]-20-[v4(40)]-20-[v5(40)]-20-[v6(40)]", views: idLabel, titleLabel, urlLabel, isFeaturedLabel, priceLabel, driveTimeLabel, addButton)
    }
    
    override func viewDidLayoutSubviews() {
        addGradientToShowResultsButton()
    }
    
    private func addGradientToShowResultsButton(){
        let layer = CAGradientLayer()
        layer.frame = view.bounds
        layer.colors = [UIColor.yellow.cgColor,
                        UIColor.green.cgColor]
        layer.startPoint = CGPoint(x: 0.5,y: 0.5)
        layer.endPoint = CGPoint(x: 0.3, y: 0.3)
        showObjectsButton.layer.insertSublayer(layer, at: 0)
    }
    
    //MARK: - Button footer
    private func addButtonView(){
        let v = UIView()
        v.layer.shadowColor = UIColor.black.cgColor
        v.layer.shadowOpacity = 1
        v.layer.shadowOffset = .zero
        v.layer.shadowRadius = 2
        v.layer.shadowPath = UIBezierPath(rect: v.bounds).cgPath
        v.backgroundColor = .black
        view.addSubview(v)
        v.addSubview(showObjectsButton)
        
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: v)
        view.addConstraintsWithFormat(format: "V:[v0(60)]|", views: v)
        v.addConstraintsWithFormat(format: "H:|[v0]|", views: showObjectsButton)
        v.addConstraintsWithFormat(format: "V:|[v0]|", views: showObjectsButton)
        
        showObjectsButton.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    let showObjectsButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 22)
        button.setTitle("POKAŻ OBIEKTY",for: .normal)
        //button.titleLabel?.textColor = UIColor.white
        return button
    }()
    
    @objc func buttonClicked(sender : UIButton){
        let desVC = ObjectListViewController(collectionViewLayout: UICollectionViewFlowLayout())
        
        navigationController?.pushViewController(desVC, animated: true)
    }
    
    @objc func handleAddButtonTap(){
        print("add")
        let objectId = Int64(idLabel.text!)
        guard let unwrappedObjectId = objectId else { return }
        
        let objectTitle: String? = titleLabel.text!
        guard let unwrappedObjectTitle = objectTitle else { return }
        
//        let objectUrl: String? = urlLabel.text!
        let objectUrl: String? = "https://www.hotelbeaufortdelhi.in/images/web/room/prm/1.jpg"
        guard let unwrappedObjectUrl = objectUrl else { return }
        
        let objectIsFeatured = Int16(isFeaturedLabel.text!)
        guard let unwrappedObjectIsFeatured = objectIsFeatured else { return }
        
        let objectPrice = Float(priceLabel.text!)
        guard let unwrappedObjectPrice = objectPrice else { return }
        
        let objectDriveTime = Float(driveTimeLabel.text!)
        guard let unwrappedObjectDriveTime = objectDriveTime else { return }
        
        let newObject = Object(id: unwrappedObjectId, title: unwrappedObjectTitle, url: unwrappedObjectUrl, isFeatured: unwrappedObjectIsFeatured, price: unwrappedObjectPrice, driveTime: unwrappedObjectDriveTime)
        ObjectManager.saveObject(object: newObject)
    }
    
    let addButton: UIButton = {
        let button = UIButton()
        button.setTitle("add", for: .normal)
        button.backgroundColor = .black
        return button
    }()
    
    let idLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj Id obiektu"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    
    let titleLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj tytuł obiektu"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    let urlLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj adres zdjęcia"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    let isFeaturedLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "czy wyróżniony: 1 - tak, 0 - nie"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    let priceLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj cene"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    
    let driveTimeLabel: PaddedTextField = {
        let textBar = PaddedTextField()
        textBar.tintColor = .red
        textBar.backgroundColor = .white
        textBar.layer.cornerRadius = 15.0
        textBar.layer.borderWidth = 0.5
        textBar.layer.borderColor = UIColor.customLightGray.cgColor
        var myMutableStringTitle = NSMutableAttributedString()
        var myString = "Podaj jaki czas do celu"
        myMutableStringTitle = NSMutableAttributedString(string: myString, attributes:
            [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14.0)!]) // Font
        myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range:NSRange(location:0,length:myString.count))
        textBar.attributedPlaceholder = myMutableStringTitle
        return textBar
    }()
    
}

