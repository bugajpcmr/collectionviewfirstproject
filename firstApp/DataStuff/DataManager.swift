//
//  DataManager.swift
//  twit
//
//  Created by Bartosz Bugajski on 05/08/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import CoreData
import UIKit

struct Object {
    let id: Int64
    let title: String
    let url: String
    var isFeatured: Int16
    let price: Float
    let driveTime: Float
}

var ObjectsArray = [Object]()

class ObjectManager: NSObject {
    
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    class func saveObject(object: Object){
        
        let fetchRequest: NSFetchRequest<ObjectEntity> = ObjectEntity.fetchRequest()
        
        var alredyExist = false
        
        do {
            let saveObject = try getContext().fetch(fetchRequest)
            
            for item in saveObject as [NSManagedObject] {
                if (item.value(forKey: "title") as! String  == object.title) || (item.value(forKey: "id") as! Int64  == object.id) || ((item.value(forKey: "id") as! Int64  == object.id) && (item.value(forKey: "title") as! String  == object.title)) {
                    alredyExist = true
                    break
                }
            }
            
            if alredyExist {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "alreadyExists"), object: nil)
            }
            else {
                saveItem(object: object)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "objectSaved"), object: nil)
            }
        }
        catch {
            print("Error with save request: \(error)")
        }
    }
    
    
    fileprivate class func saveItem(object: Object){
        
        let context = getContext()
        
        let entity = NSEntityDescription.entity(forEntityName: "ObjectEntity", in: context)
        let managedObjc = NSManagedObject(entity: entity!, insertInto: context)
        
        managedObjc.setValue(object.id, forKey: "id")
        managedObjc.setValue(object.title, forKey: "title")
        managedObjc.setValue(object.url, forKey: "url")
        managedObjc.setValue(object.isFeatured, forKey: "isFeatured")
        managedObjc.setValue(object.price, forKey: "price")
        managedObjc.setValue(object.driveTime, forKey: "driveTime")
        
        do {
            try context.save()
            print("saved!")
        } catch {
            print(error.localizedDescription)
        }
        
        getObjects()
    }
    
    class func deleteItem(id: Int64, title: String){
        let context = getContext()
        let fetchRequest: NSFetchRequest<ObjectEntity> = ObjectEntity.fetchRequest()
        
        do {
            let savedTimes = try getContext().fetch(fetchRequest)
            
            for item in savedTimes as [NSManagedObject] {
                if (item.value(forKey: "id") as! Int64 == id) && (item.value(forKey: "title") as! String == title){
                    context.delete(item)
                    break
                }
            }
            do {
                try context.save()
                print("Deleted!")
                getObjects()
            }
            catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
        catch {
            print("Error with request: \(error)")
        }
    }
    
    
    class func getObjects() {
        let fetchRequest: NSFetchRequest<ObjectEntity> = ObjectEntity.fetchRequest()
        ObjectsArray.removeAll()
        
        do {
            let fetchResult = try getContext().fetch(fetchRequest)
            
            for item in fetchResult {
                let newElement: Object = Object(id: item.id, title: item.title ?? "", url: item.url ?? "", isFeatured: item.isFeatured, price: item.price, driveTime: item.driveTime)
                ObjectsArray.append(newElement)
            }
            //ObjectsArray = ObjectsArray.reversed()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        }
        catch {
            print("error LastSearch in data manager")
        }
    }
    
    class func isObjectsEmpty() -> Bool {
        do {
            let request:NSFetchRequest<ObjectEntity> = ObjectEntity.fetchRequest()
            let count  = try getContext().count(for: request)
            return count == 0 ? true : false
        }
        catch {
            return true
        }
    }
    
}


