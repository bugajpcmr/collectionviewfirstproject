//
//  CollectionViewController.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 10/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import Foundation
//import Alamofire
import UIKit


class CollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let headerCellId = "headercell"
    let firstCellId = "firstcell"
    let titleCellId = "titlecell"
    let roomCellId = "roomcell"
    let adressCellId = "adresscell"
    let numberCellId = "numbercell"
    let ownerCellId = "ownercell"
    let infoCellId = "infocell"
    let mapCellId = "mapcell"
    let galeryCellId = "galerycell"
    let advancedGaleryCellId = "advancedgalerycell"
    let horizontalCellId = "horizontalcell"
    
    let photos = ["room_1", "room_2", "object_2", "object_1", "google"]
    let headerNamesArray = ["","","WŁAŚCICIEL OBIEKTU","O OBIEKCIE","UDOGODNIENIA","MAPA", "POKOJE W TYM OBIEKCIE", "PODOBNE OFERTY","UDOGODNIENIA","UDOGODNIENIA","","","",""]
    let icons = ["navigation_circle", "phone_circle"]
    let adressNamesArray = ["ul. Myślęcin 16F","Elbląg","woj. Łódzkie"]
    let phoneNumbersArray = ["+48 455 654 706","+48 697 403 229"]
    let ownersNames = ["Grzegorz Brzęczyszczykiewicz"]
    
    var pickedObject = 1
    
    let longDescription = "Wariacji na temat carpaccio znamy co najmniej kilka: carpaccio z łososia, z tuńczyka, z cielęciny czy carpaccio owocowe. Jedną z popularniejszych wersji tej przystawki jest carpaccio z wołowiny, które można zastąpić… w czasie wykwintnej kolacji we włoskim stylu. Wariacji na temat carpaccio znamy co najmniej kilka: carpaccio z łososia, z tuńczyka, z cielęciny czy carpaccio owocowe. Jedną z popularniejszych wersji tej przystawki jest carpaccio z wołowiny, które można zaserwować w czasie wykwintnej kolacji we włoskim stylu."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ObjectManager.getObjects()
        setupCollectionView()
        registerCells()
        addButtonView()
    }
    
    private func setupCollectionView(){
        navigationItem.title = "Obiekt"
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 70, right: 0)
        navigationItem.backBarButtonItem = .none
    }
    
    override func viewDidLayoutSubviews() {
        addGradientToCallButton()
    }
    
    private func addGradientToCallButton(){
        let layer = CAGradientLayer()
        layer.frame = view.bounds
        layer.colors = [UIColor(red: 254/255, green: 140/255, blue: 0/255, alpha: 1).cgColor,
                        UIColor(red: 248/255, green: 54/255, blue: 0/255, alpha: 1).cgColor]
        layer.startPoint = CGPoint(x: 0.0,y: 0.0)
        layer.endPoint = CGPoint(x: 0.3, y: 0.3)
        callButton.layer.insertSublayer(layer, at: 0)
    }
    
    //MARK: - Button - send - footer
    private func addButtonView(){
        let v = UIView()
        v.backgroundColor = .white
        v.layer.addShadow()
        view.addSubview(v)
        v.addSubview(priceLabel)
        v.addSubview(callButton)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: v)
        view.addConstraintsWithFormat(format: "V:[v0(60)]|", views: v)
        v.addConstraintsWithFormat(format: "H:|[v0(\(view.frame.width/2))]", views: priceLabel)
        v.addConstraintsWithFormat(format: "V:|[v0]|", views: priceLabel)
        v.addConstraintsWithFormat(format: "H:[v0(\(view.frame.width/2))]|", views: callButton)
        v.addConstraintsWithFormat(format: "V:|[v0]|", views: callButton)
        
        callButton.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    @objc func buttonClicked(sender : UIButton){
        print("Calling...")
    }
    
    let callButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 15)
        button.setTitle("ZADZWOŃ",for: .normal)
        button.titleLabel?.textColor = UIColor.white
        button.layer.addShadow()
        return button
    }()
    
    let priceLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.textColor = UIColor(red: 56/255, green: 204/255, blue: 102/255, alpha: 1)
        textLabel.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        textLabel.font = .boldSystemFont(ofSize: 16)
        textLabel.text = "od 35 zł/os"
        textLabel.textAlignment = .center
        textLabel.layer.addShadow()
        return textLabel
    }()
    
    //MARK: - Register Cells
    private func registerCells(){
        collectionView.register(StretchyHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId)
        collectionView.register(FirstCell.self, forCellWithReuseIdentifier: firstCellId)
        collectionView.register(TitleCell.self, forCellWithReuseIdentifier: titleCellId)
        collectionView.register(RoomCell.self, forCellWithReuseIdentifier: roomCellId)
        collectionView.register(NumberCell.self, forCellWithReuseIdentifier: numberCellId)
        collectionView.register(OwnerCell.self, forCellWithReuseIdentifier: ownerCellId)
        collectionView.register(AdressCell.self, forCellWithReuseIdentifier: adressCellId)
        collectionView.register(InfoCell.self, forCellWithReuseIdentifier: infoCellId)
        collectionView.register(MapCell.self, forCellWithReuseIdentifier: mapCellId)
        collectionView.register(GaleryCell.self, forCellWithReuseIdentifier: galeryCellId)
        collectionView.register(HorizontalCell.self, forCellWithReuseIdentifier: horizontalCellId)
        collectionView.register(AdvancedGaleryCell.self, forCellWithReuseIdentifier: advancedGaleryCellId)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if section == 6 {
            return UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        }
        else {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    //MARK: - Sections and rows
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: firstCellId, for: indexPath) as! FirstCell
            cell.titleLabel.text = "Dostępny teraz!"
            cell.iconHeartImage.image = UIImage(named: icons[1])
            cell.iconWeightImage.image = UIImage(named: icons[0])
            cell.imageLabel.sd_setImage(with: URL(string: ObjectsArray[pickedObject].url), placeholderImage: UIImage(named: "room_1"))
            return cell
        case 1:
            switch indexPath.row {
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: titleCellId, for: indexPath) as! TitleCell
                cell.titleLabel.text = ObjectsArray[pickedObject].title
                return cell
            case 1:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: roomCellId, for: indexPath) as! RoomCell
                cell.titleLabel.text = "6 pokoi na 35 osób"
                return cell
            case 2:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: adressCellId, for: indexPath) as! AdressCell
                cell.streetLabel.text = adressNamesArray[0]
                cell.cityLabel.text = adressNamesArray[1]
                cell.distrLabel.text = adressNamesArray[2]
                cell.iconImage.image = UIImage(named: icons[0])
                return cell
            case 3:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: numberCellId, for: indexPath) as! NumberCell
                cell.phoneNumberOneLabel.text = phoneNumbersArray[0]
                cell.phoneNumberTwoLabel.text = phoneNumbersArray[1]
                cell.iconImage.image = UIImage(named: icons[1])
                return cell
            default:
                return UICollectionViewCell()
            }
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ownerCellId, for: indexPath) as! OwnerCell
            cell.titleLabel.text = ownersNames[0]
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: infoCellId, for: indexPath) as! InfoCell
            cell.titleLabel.text = longDescription
            return cell
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: horizontalCellId, for: indexPath) as! HorizontalCell
            return cell
        case 5:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mapCellId, for: indexPath) as! MapCell
            cell.imageView.image = UIImage(named: photos[4])
            return cell
        case 6:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: galeryCellId, for: indexPath) as! GaleryCell
            cell.imageView.image = UIImage(named: photos[indexPath.row])
            cell.titleLabel.text = "pokój na 6 osób"
            return cell
        case 7:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: advancedGaleryCellId, for: indexPath) as! AdvancedGaleryCell
            cell.imageView.sd_setImage(with: URL(string: ObjectsArray[indexPath.row].url), placeholderImage: UIImage(named: "room_1"))
            cell.imageTitleLabel.text = "\(ObjectsArray[indexPath.row].driveTime) minut drogi"
            if ObjectsArray[indexPath.row].isFeatured == 1 {
                cell.iconHeartImage.setImage(UIImage(named: "heart_solid"), for: .normal)
            }
            cell.titleLabel.text = ObjectsArray[indexPath.row].title
            cell.priceLabel.text = "\(ObjectsArray[indexPath.row].price) zł/os"
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    //View change
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath)
        
        if indexPath.section == 3 {
            openDescriptionViewController()
        }
        if indexPath.section == 4 {
            openHorizontalViewController()
        }
    }
    private func openDescriptionViewController(){
        let desVC = ExtendedDescriptionViewController()
        desVC.descriptionText = longDescription
        navigationController?.pushViewController(desVC, animated: true)
//        desVC.modalPresentationStyle = .fullScreen
//        present(desVC, animated: true, completion: nil)
    }
    
    private func openHorizontalViewController() {
        let layout = UICollectionViewFlowLayout()
        let desVC = ExtendedHorizontalViewController(collectionViewLayout: layout)
        navigationController?.pushViewController(desVC, animated: true)
    }
    
    //MARK: - How many rows in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 4
        case 6:
            return 2
        case 7:
            return ObjectsArray.count
        default:
            return 1
        }
    }
    
    //MARK: - how many sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 8
    }
    
    //MARK: - sections and rows height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width, height: 200)
        case 1:
            switch indexPath.row {
            case 0:
                return CGSize(width: view.frame.width, height: 70)
            case 1:
                return CGSize(width: view.frame.width, height: 58)
            case 2:
                return CGSize(width: view.frame.width, height: 102)
            case 3:
                return CGSize(width: view.frame.width, height: 86)
            default:
                return CGSize(width: view.frame.width, height: 70)
            }
        case 2:
            return CGSize(width: view.frame.width, height: 50)
        case 3:
            return CGSize(width: view.frame.width, height: 160)
        case 4:
            return CGSize(width: view.frame.width, height: 76)
        case 5:
            return CGSize(width: view.frame.width, height: 200)
        case 6:
            return CGSize(width: view.frame.width - 32, height: 185)
        case 7:
            return CGSize(width: view.frame.width, height: 245)
        default:
            return CGSize(width: view.frame.width, height: 50)
        }
    }
    
    //MARK: - paddings
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if section == 6 {
            return 16
        }
        else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - section HEADERS
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! StretchyHeaderCell
        header.titleLabel.text = headerNamesArray[indexPath.section]
        return header
    }
    
    //MARK: - Headers - height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case 0:
            return CGSize(width: view.frame.width, height: 0)
        case 1:
            return CGSize(width: view.frame.width, height: 0)
        case 8:
            return CGSize(width: view.frame.width, height: 0)
        default:
            return CGSize(width: view.frame.width, height: 50)
        }
    }
    
}


