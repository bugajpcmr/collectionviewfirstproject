//
//  AmenitiesCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 26/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class AmenitiesCell: UICollectionViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCollectionView()
        setupViews()
    }
    
    let elementsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    var iconsAndTitlesArray = [
        IconAndTitle(iconName: "wifi", title: "wifi")]
    
    let singleCellId = "singlecellId"
    
    private func setupCollectionView(){
        elementsCollectionView.dataSource = self
        elementsCollectionView.delegate = self
        elementsCollectionView.register(SingleCell.self, forCellWithReuseIdentifier: singleCellId)
        elementsCollectionView.isUserInteractionEnabled = false
    }
    
    let imageIconArrow: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "navigation_circle")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    
    func setupViews(){
        //backgroundColor = .black
        addSubview(elementsCollectionView)
        addSubview(imageIconArrow)
        addConstraintsWithFormat(format: "H:|[v0]|", views: elementsCollectionView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: elementsCollectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        if iconNamesArray.count < 6 {
        //            return iconNamesArray.count
        //        }
        //        else{
        //            return 6
        //        }
        return iconsAndTitlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: singleCellId, for: indexPath) as! SingleCell
        cell.imageIcon.image = UIImage(named: iconsAndTitlesArray[indexPath.row].iconName)
        cell.titleLabel.text = iconsAndTitlesArray[indexPath.row].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 40)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SingleCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageIcon: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(named: "navigation_circle")
        imager.contentMode = ContentMode.scaleAspectFit
        return imager
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 11)
        return label
    }()
    
    func setupViews(){
        addSubview(imageIcon)
        addSubview(titleLabel)
        addConstraintsWithFormat(format: "V:|[v0(40)]|", views: imageIcon)
        
        addConstraintsWithFormat(format: "H:|[v0(40)]-6-[v1]", views: imageIcon, titleLabel)
        addConstraintsWithFormat(format: "V:|[v0]|", views: titleLabel)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



