//
//  PriceMaxCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 25/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class PriceMaxCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 56/255, green: 204/255, blue: 102/255, alpha: 1)
        label.font = UIFont(name: "SF Pro Display", size: 11)
        label.font = .boldSystemFont(ofSize: 13)
        return label
    }()
    
    func setupViews(){
        addSubview(priceLabel)
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: priceLabel)
        addConstraintsWithFormat(format: "V:|-16-[v0]-16-|", views: priceLabel)
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

