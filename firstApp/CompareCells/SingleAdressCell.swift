//
//  SingleAdressCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 25/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class SingleAdressCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let streetLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.font = UIFont(name: "SF Pro Display", size: 11)
        label.font = .systemFont(ofSize: 11)
        return label
    }()
    
    let cityLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.font = UIFont(name: "SF Pro Display", size: 11)
        label.font = .systemFont(ofSize: 11)
        return label
    }()
    
    let distrLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.font = UIFont(name: "SF Pro Display", size: 11)
        label.font = .systemFont(ofSize: 11)
        return label
    }()
    
    func setupViews(){
        backgroundColor = .white
        addSubview(streetLabel)
        addSubview(cityLabel)
        addSubview(distrLabel)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: streetLabel)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: cityLabel)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: distrLabel)
        
        addConstraintsWithFormat(format: "V:|-16-[v0]-6-[v1]-6-[v2]", views: streetLabel, cityLabel, distrLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
