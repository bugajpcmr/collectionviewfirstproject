//
//  RoomsCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 26/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class RoomsCell: UICollectionViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCollectionView()
        setupViews()
    }
    
    let elementsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    var photosArray = ["room_1","room_2","room_3"]
    
    let roomCellId = "singlecellId"
    
    private func setupCollectionView(){
        elementsCollectionView.dataSource = self
        elementsCollectionView.delegate = self
        elementsCollectionView.register(SingleRoomCell.self, forCellWithReuseIdentifier: roomCellId)
        elementsCollectionView.isUserInteractionEnabled = false
    }

    func setupViews(){
        //backgroundColor = .black
        addSubview(elementsCollectionView)
        addConstraintsWithFormat(format: "H:|[v0]|", views: elementsCollectionView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: elementsCollectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: roomCellId, for: indexPath) as! SingleRoomCell
        cell.imageView.image = UIImage(named: photosArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 101)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SingleRoomCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageView: UIImageView = {
        let imager = UIImageView()
        imager.contentMode = ContentMode.scaleAspectFill
        imager.clipsToBounds = true
        imager.layer.masksToBounds = true
        imager.layer.cornerRadius = 6
        return imager
    }()
    
    func setupViews(){
        addSubview(imageView)
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: imageView)
        addConstraintsWithFormat(format: "V:|-16-[v0]|", views: imageView)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
