//
//  TopPhotoCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 25/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class TopPhotoCell: UICollectionViewCell {
    
    var myCollectionViewController: CompareCollectionViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let imageView: UIImageView = {
        let imager = UIImageView()
        imager.contentMode = ContentMode.scaleAspectFill
        imager.clipsToBounds = true
        imager.layer.masksToBounds = true
        imager.layer.cornerRadius = 6
        return imager
    }()
    
    let iconCancelImage: UIButton = {
        let imager = UIButton()
        imager.contentMode = ContentMode.scaleAspectFill
        return imager
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "SF Pro Display", size: 13)
        label.font = .systemFont(ofSize: 13)
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.numberOfLines = 0
        return label
    }()
    
    func setupViews(){
        addSubview(imageView)
        addSubview(iconCancelImage)
        addSubview(titleLabel)
        backgroundColor = .white
        addConstraintsWithFormat(format: "H:[v0(24)]-23-|", views: iconCancelImage)
        addConstraintsWithFormat(format: "V:|-20-[v0(24)]", views: iconCancelImage)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: imageView)
        addConstraintsWithFormat(format: "V:|-60-[v0(84)]", views: imageView)
        
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: titleLabel)
        addConstraintsWithFormat(format: "V:[v0]-16-[v1]", views: imageView, titleLabel)
        
        iconCancelImage.addTarget(self, action: #selector(handleObjectDeletion), for: .touchUpInside)
    }
    
    @objc func handleObjectDeletion(){
        myCollectionViewController?.tapCancelIcon(cell: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
