//
//  CapacityMaxCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 25/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class CapacityMaxCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let capacityLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        let attributedString = NSMutableAttributedString(string: "Your text")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        label.font = UIFont(name: "SF Pro Display", size: 11)
        label.font = .systemFont(ofSize: 11)
        label.numberOfLines = 0
        return label
    }()
    
    func setupViews(){
        backgroundColor = .white
        addSubview(capacityLabel)
        addConstraintsWithFormat(format: "H:|-16-[v0]-30-|", views: capacityLabel)
        addConstraintsWithFormat(format: "V:|-16-[v0]-16-|", views: capacityLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

