//
//  EmptyObjectCell.swift
//  firstApp
//
//  Created by Bartosz Bugajski on 31/07/2019.
//  Copyright © 2019 Bartosz Bugajski. All rights reserved.
//

import UIKit

class EmptyObjectCell: UICollectionViewCell {
    
    var myCollectionViewController: CompareCollectionViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "SF Pro Display", size: 11)
        label.font = .boldSystemFont(ofSize: 15)
        label.textAlignment = .center
        return label
    }()

    let plusLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        //label.font = UIFont(name: "SF Pro Display", size: 11)
        label.font = .boldSystemFont(ofSize: 40)
        label.text = "+"
        label.textAlignment = .center
        label.layer.masksToBounds = true

        return label
    }()
    
    let iconAddObject: UIButton = {
        let imager = UIButton()
        imager.backgroundColor = .orange
        imager.contentMode = ContentMode.scaleAspectFit
        imager.layer.masksToBounds = true
        imager.layer.cornerRadius = 40
        return imager
    }()
    
    func setupViews(){
        addSubview(titleLabel)
        addSubview(iconAddObject)
        iconAddObject.addSubview(plusLabel)
        backgroundColor = .white
        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: titleLabel)
        //addConstraintsWithFormat(format: "V:|-16-[v0]-16-|", views: titleLabel)
        
        addConstraintsWithFormat(format: "H:[v0(80)]", views: iconAddObject)
        addConstraintsWithFormat(format: "V:|-30-[v0]-32-[v1(80)]", views: titleLabel, iconAddObject)
        
        iconAddObject.addConstraintsWithFormat(format: "H:|-10-[v0]-10-|", views: plusLabel)
        plusLabel.centerYAnchor.constraint(equalTo: iconAddObject.centerYAnchor, constant: -2).isActive = true
        
//        iconAddObject.addConstraintsWithFormat(format: "V:|[v0]|", views: plusLabel)
        iconAddObject.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        iconAddObject.addTarget(self, action: #selector(handleObjectAddition), for: .touchUpInside)
    }
    
    @objc func handleObjectAddition(){
        myCollectionViewController?.tapAddIcon(cell: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
